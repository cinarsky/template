export var x = 8

var y = 9
var z = 10

export { y, z }

export function inc() {
  y++
}

export default function (a,b) {
  return a + b
}

//静态分析(不运行代码的情况下进行分析，如尾递归优化)