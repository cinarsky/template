function assert(test, msg = 'Boom') {
  if (!test) {
    throw new Error(msg)
  }
}

function Vector(x, y) {
  this.x = x
  this.y = y
}

Vector.prototype.plus = function (v) {
  return new Vector(this.x + v.x, this.y + v.y)
}

describe('Array', function (params) {
  describe('Array constructor', function (params) {
    describe('xxxx', function (params) {
      类库 Lodash
    })
    describe('yyyy', function (params) {
      
    })
  })
  describe('Array methods', function (params) {
    
  })
})


//只适合于测需求变更不频繁的东西
describe('Vector', function(){
  it('should construct a Verctor instance when called by new', function(){
    var a = new Vector(1,2)
    assert(a.x == 1)
    assert(a.y == 2)
  })
  it('should return a new Vector when plus with other', function(){
    var a = new Vector(1,2)
    var b = new Vector(3,4)
    var c = a.plus(b)

    assert(c !== a, 'xxxxx')
    assert(c !== b)
    assert(c.x == 4)
    assert(c.y == 6)
  })
})



function foo() {
  throw 8
}

try {
  foo()
} catch(e) {

} finally {

}

function hasValue(root, val) {

  try {
    traverse(root)
    return false
  } catch(e) {
    if (e === true) {
      return true
    } else {
      throw e
    }
  }

  function traverse(root) {
    if (root) {
      if (root.val == val) {
        throw new Error('osdifjwoef')
      }
      traverse(root.left)
      traverse(root.right)
    }
  }
}

e.stack
e.message
e.name

e instanceof TypeError
e.name === TypeError

TypeError
ReferenceError
RangeError
SyntaxError


class UserLoginError extends Error {
  constructor(message) {
    super(message)
    this.name = 'UserLoginError'
  }
}

JSLint
JSHint

ESLint


class MultiplicatorUnitFailure extends Error {
}

function primitiveMultiply(a, b) {
  if (Math.random() > 0.5) {
    return a * b
  } else {
    throw new MultiplicatorUnitFailure('xxx')
  }
}

function mul(a, b) {
  while(true) {
    try {
      return primitiveMultiply(a, b)
    } catch(e) {
      if (e instanceof MultiplicatorUnitFailure) {
        continue
      } else {
        throw e
      }
    }
  }
}



var box = {

}

function withBoxUnlocked(f) {
  box.unlock()
  try {
    f()
  } finally {
    box.lock()
  }
}



withBoxUnlocked(function(xxx) {
  
})


f1.__close__()






with open('/path/to/file', 'r') as f1, open('foo.txt') as f2:
    print(f.read())
    return 8

////////////

function pyWith(...args) {
  var action = args.pop()
  try {
    action(...args)
  } finally {
    for(var file of files) {
      file.close()
    }
  }
}

pyWith(open('a.txt'), open('b.txt'),  function(fa, fb) {
  fa.read(3)
  fb.read(4)
})
