const fs = require('fs')
const multer = require('multer')
const md5 = require('md5')
const express = require('express')
const sharp = require('sharp')
const svgCaptcha = require('svg-captcha')
const fsp = fs.promises
const uploader = multer({
  dest: './upload/',
  preservePath: true,
})

let db
const dbPromise = require('./db')
dbPromise.then(dbObject => {
  db = dbObject
})

const changePasswordTokenMap = {}
const mailer = require('./mailer')

const app = express.Router()

app.route('/register')
  .get((req, res, next) => {
    res.send(`
      <form action="/register" method="post" enctype="multipart/form-data">
        用户名：<input type="text" name="name"/><br>
        邮箱：<input type="text" name="email"/><br>
        密码：<input type="password" name="password"/><br>
        头像：<input multiple type="file" name="avatar" /><br>
        <button>注册</button>
      </form>
    `)
  })
  .post(uploader.single('avatar'), async (req, res, next) => {
    var regInfo = req.body
    console.log('avatar', req.file)

    var imgBuf = await fsp.readFile(req.file.path)

    await sharp(imgBuf)
      .resize(256)
      .toFile(req.file.path)

    var user = await db.get('SELECT * FROM users WHERE name=?', regInfo.name)
    var user = await db.getUser(id)
    if (user) {
      res.end('用户名已被占用')
    } else {
      await db.run('INSERT INTO users (name, email, password, avatar) VALUES (?,?,?,?)',
        regInfo.name, regInfo.email, md5(md5(regInfo.password)), req.file.path
      )
      res.end('注册成功')
    }
  })


app.get('/captcha', (req, res, next) => {
  var captcha = svgCaptcha.create({
    ignoreChars: '0o1il'
  })
  res.type('svg')
  req.session.captcha = captcha.text
  res.end(captcha.data)
})

app.route('/login')
  .get((req, res, next) => {
    res.send(`
      <form id="loginForm" action="/login" method="post">
        用户名：<input type="text" name="name"/><br>
        密码：<input type="password" name="password"/><br>
        验证码：<input type="text" name="captcha" /> <img id="captchaImg" src="/captcha" /><br>
        <a href="/forgot">忘记密码</a>
        <button>登陆</button>
      </form>

      <script>
      
        captchaImg.onclick = () => {
          captchaImg.src = '/captcha?' + Date.now()
        }
       
        loginForm.onsubmit = e => {
          e.preventDefault()
          var name = document.querySelector('[name="name"]').value
          var password = document.querySelector('[name="password"]').value
          var captcha = document.querySelector('[name="captcha"]').value

          var xhr = new XMLHttpRequest()
          xhr.open('POST', '/login')
          xhr.onload = () => {
            var data = JSON.parse(xhr.responseText)
            if (data.code == 0) {
              alert('login success, will redirected to homepage')
              location.href = '/'
            } else {
              alert('login failed')
              captchaImg.click()
            }
          }
          xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
          xhr.send('name=' + name + '&password=' + password + '&captcha=' + captcha)
        }
      </script>
    `)
  })
  .post(async (req, res, next) => {
    var tryLoginInfo = req.body

    if (tryLoginInfo.captcha != req.session.captcha) {
      res.json({code: -1, msg: '验证码错误'})
      return
    }
    var user = await db.get('SELECT * FROM users WHERE name=? AND password=?',
      tryLoginInfo.name, md5(md5(tryLoginInfo.password))
    )
    if (user) {
      res.cookie('userid', user.id, {
        signed: true
      })
      res.json({code: 0})
    } else {
      res.json({code: -1, msg: '用户名或密码错误'})
    }
  })

app.route('/forgot')
  .get((req, res, next) => {
    res.end(`
      <form action="/forgot" method="post">
        请输入您的邮箱：<input type="text" name="email"/>
        <button>确定</button>
      </form>
    `)
  })
  .post(async (req, res, next) => {
    var email = req.body.email
    var user = await db.get('SELECT * FROM users WHERE email=?', email)
    if (!user) {
      res.end('不存在此用户')
    }

    var token = Math.random().toString().slice(2)

    changePasswordTokenMap[token] = email

    setTimeout(() => {
      delete changePasswordTokenMap[token]
    }, 60 * 1000 * 20)//20分钟后删除token

    var link = `http://localhost:3005/change-password/${token}`

    console.log(link)

    mailer.sendMail({
      from: '285696737@qq.com',
      to: email,
      subject: '密码修改',
      text: link
    }, (err, info) => {
      res.end('已向您的邮箱发送密码重置链接，请于20分钟内点击链接修改密码！')
    })
  })

app.route('/change-password/:token')
  .get(async (req, res, next) => {
    var token = req.params.token
    var name = changePasswordTokenMap[token]

    if (!name) {
      res.end('链接已失效')
      return
    }

    // var user = await db.get('SELECT * FROM users WHERE')

    res.end(`
      此页面可以重置${name}的密码
      <form action="" method="post">
        <input type="password" name="password"/>
        <button>提交</button>
      </form>
    `)
  })
  .post(async (req, res, next) => {
    var token = req.params.token
    var email = changePasswordTokenMap[token]
    var password = req.body.password

    if (!email) {
      res.end('链接已失效')
      return
    }

    delete changePasswordTokenMap[token]
    
    await db.run('UPDATE users SET password=? WHERE email=?', md5(md5(password)), email)

    res.end('密码修改成功')
  })



app.get('/logout', (req, res, next) => {
  res.clearCookie('userid')
  res.redirect('/')
})


module.exports = app