const express = require('express')
const socketIO = require('socket.io')
const cookieParser = require('cookie-parser')
const http = require('http')
const url = require('url')
const session = require('express-session')


const app = express()
const server = http.createServer(app)
const ioServer = socketIO(server)


/**
 * TODO:
 * 用户密码是不能明文存储  md5(md5(md5(password)))
 * 实时更新投票数据
 * 未登陆不能为非匿名问题投票
 *    以及相关的其它权限验证
 * 问题过期后不能再投票，只能查看结果
 *    问题过期后投票结果也不会再实时更新，所以不需要建立socketio连接
 * 各个页面的交互，不能只返回一个由文字组成的页面
 * 整站的所有页面都有头部和底部（用模板）
 * 创建投票的页面交互优化
 * 创建投票的后端需要额外的验证：
 *    验证选项的数量（大于等于2）
 *    过期时间需要是未来时间点
 * 页面UI的优化（建议使用Bootstrap）
 * 登陆后可以查看自己发起过的投票
 * 各种数据库操作出错的时候要对前端有正确的返回
 * 
 * 
 * 登陆时需要输入验证码
 * 注册时可以上传头像
 */


const port = 3005

app.locals.pretty = true//美化模板输出

app.set('views', __dirname + '/tpl')
// app.set('view engine', 'pug')




const userAccountRouter = require('./user-account')


ioServer.on('connection', socket => {
  var path = url.parse(socket.request.headers.referer).path
  socket.join(path)
})



app.use((req, res, next) => {
  console.log(req.method, req.originalUrl)
  res.set('Content-Type', 'text/html; charset=UTF-8')
  next()
})


app.use(session({secret: 'my secret', resave: false, cookie: { maxAge: 60000 }}))

app.use(cookieParser('my secret'))

app.use(express.static(__dirname + '/static'))
app.use('/upload', express.static(__dirname + '/upload'))

//解析json请求体的中间件
app.use(express.json())
//解析url编码的中间件
app.use(express.urlencoded({
  extended: true
}))

app.get('/', async (req, res, next) => {
  // console.log(req.cookies)
  // console.log(req.signedCookies)

  if (req.signedCookies.userid) {

    var user = await db.get('SELECT * FROM users WHERE id=?', req.signedCookies.userid)

    res.render('index.pug', {
      user: user
    })
  } else {
    res.send(`
      <div>
        <a href="/register">注册</a>
        <a href="/login">登陆</a>
      </div>
    `)
  }
})

app.get('/create', (req, res, next) => {
  res.render('create.pug')
})

app.post('/create-vote', async (req, res, next) => {
  var voteInfo = req.body
  var userid = req.signedCookies.userid
  console.log(req.body)
  console.log(req.signedCookies.userid)

  // name = 'foo" OR "1"="1'
  // await db.get(`SELECT * FROM users WHERE name="${name}"`)

  await db.run('INSERT INTO votes (title, desc, userid, singleSelection, deadline, anonymouse) VALUES (?,?,?,?,?,?)',
    voteInfo.title, voteInfo.desc, userid, voteInfo.singleSelection, new Date(voteInfo.deadline).getTime(), voteInfo.anonymouse
  )
  
  var vote = await db.get('SELECT * FROM votes ORDER BY id DESC LIMIT 1')

  await Promise.all(voteInfo.options.map(option => {
    return db.run('INSERT INTO options (content, voteid) VALUES (?,?)', option, vote.id)
  }))
  
  res.redirect('/vote/' + vote.id)
})

app.get('/vote/:id', async (req, res, next) => {
  var votePromise = db.get('SELECT * FROM votes WHERE id=?', req.params.id)
  var optionsPromise = db.all('SELECT * FROM options WHERE voteid=?', req.params.id)

  var vote = await votePromise
  var options = await optionsPromise

  res.render('vote.pug', {
    vote: vote,
    options: options
  })
})

//用户投票
app.post('/voteup', async (req, res, next) => {
  var userid = req.signedCookies.userid
  var body = req.body
  var voteid = body.voteid

  
  var voteupInfo = await db.get('SELECT * FROM voteups WHERE userid=? AND voteid=?', userid, body.voteid)
  
  if (voteupInfo) {//已经投过
    // return res.end()
    await db.run('UPDATE voteups SET optionid=? WHERE userid=? AND voteid=?', body.optionid, userid, body.voteid)
  } else {
    await db.run('INSERT INTO voteups (userid, optionid, voteid) VALUES (?,?,?)',
      req.signedCookies.userid, req.body.optionid, req.body.voteid
    )
  }

  ioServer.in(`/vote/${voteid}`).emit('new vote', {
    userid,
    voteid,
    optionid: body.optionid,
  })

  var voteups = await db.all('SELECT * FROM voteups WHERE voteid=?', req.body.voteid)
  res.json(voteups)
})

//某个用户获取某个问题的投票信息
app.get('/voteup/:voteid/info', async(req, res, next) => {
  var userid = req.signedCookies.userid
  var voteid = req.params.voteid
  var userVoteupInfo = await db.get('SELECT * FROM voteups WHERE userid=? AND voteid=?', userid, voteid)

  if (userVoteupInfo) {
    var voteups = await db.all('SELECT * FROM voteups WHERE voteid=?', voteid)
    res.json(voteups)
  } else {
    res.json(null)
  }
})

app.use('/', userAccountRouter)




let db
const dbPromise = require('./db')

dbPromise.then(dbObject => {
  db = dbObject
  server.listen(port, () => {
    console.log('server listening on port', port)
  })
})