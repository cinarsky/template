
const socketIO = require('socket.io')
const http = require('http')

const server = http.createServer()

const ioServer = socketIO(server)

ioServer.on('connection', ws => {
  console.log('someone come in...')

  ws.on('hello', data => {
    console.log('ws data', data)
    ws.emit('hello', {a:1})
  })

})

server.on('request', (req, res) => {
  console.log(req.method, req.url)
  if (req.method == 'GET' && req.url == '/') {
    res.end(`
      <script src="/socket.io/socket.io.js"></script>
      <script>
        // var socket = io();
      </script>
    `)
  }
})



server.listen(3007,'127.0.0.1', () => console.log(3007))

