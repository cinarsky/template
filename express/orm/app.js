async function main() {

  const Sequelize = require('sequelize');

  // Option 1: Passing parameters separately
  const db = new Sequelize({
    storage: './db.sqlite3',
    dialect: 'sqlite'
  });


  const User = db.define('user', {
    // attributes
    firstName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    lastName: {
      type: Sequelize.STRING
      // allowNull defaults to true
    }
  }, {
    // options
  });

  const Post = db.define('post', {
    title: Sequelize.STRING,
    userid: User.id
  })


  await User.sync({force: false})

  var users = await User.findAll()

  users[0].lastName = 'Miao'
  users[0].save()

  
}


main()