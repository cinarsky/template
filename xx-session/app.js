const express = require('express')
const session = require('express-session')
const cookie = require('cookie-parser')
const svgCaptcha = require('svg-captcha')

const app = express()

app.use(express.urlencoded({extended: true}))

app.use(cookie('foo'))
app.use(session({
  secret: 'foo',
}))

app.route('/')
  .get(async (req, res, next) => {
    res.type('html')
      .end(`
        <form action="" method="post">
          <img src="/captcha" />
          <input name="captcha" />
          <button>submit</button>
        </form>  
      `)
  })
  .post(async (req, res, next) => {
    console.log('----------', req.session, req.cookies, req.signedCookies)
    console.log(req.session.captcha, req.body.captcha)
    if (req.session.captcha == req.body.captcha) {
      res.end('ok')
    } else {
      res.end('not ok')
    }
  })

app.get('/captcha', async (req, res, next) => {
  var c = svgCaptcha.create()
  req.session.captcha = c.text
  res.type('svg').end(c.data)
})

app.listen(8081, () => console.log(8081))

