const express = require('express')

const port = 3010

const app = express()

app.use(express.static('./static/'))

app.use('/new-product/vue-app', (req, res, next) => {
  res.sendFile(__dirname + '/vue-router-embed.html')
})

app.listen(port, () => {
  console.log(port)
})
