import Vue from 'vue';
import Router from 'vue-router';

import Index from './views/Index.vue';
import CreateVote from './views/CreateVote.vue';
import ViewVote from './views/ViewVote.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';

Vue.use(Router);

export default new Router({
  routes: [{
    path: '/',
    component: Index,
  }, {
    path: '/create',
    component: CreateVote,
  }, {
    path: '/vote/:id',
    component: ViewVote,
  }, {
    path: '/login',
    component: Login,
  }, {
    path: '/register',
    component: Register,
  }, {
    path: '/forgot',
    component: Login,
  }, {
    path: '/change-password/:token',
    component: Login,
  }],
});
