const net = require('net')
const fs = require('fs')


const server = net.createServer(conn => {
  conn.on('error', () => {})
  
  process.stdin.pipe(conn)
  conn.pipe(process.stdout)
})

server.listen(9999, () => {
  console.log(server.address())
})


