const stream = require('stream')
const { Readable, Writable, Duplex, Transform } = stream


class Compress extends Transform {

  _transform(chunk, encoding, callback) {
    this.push(process(chunk))
  }

}


class TCPConnect extends Duplex {
  constructor() {

  }
  _read(size) {//流的使用者需要数据时
    this.push()
  }
  _write(chunk, encoding, done) {//此流被其它人定入数据时，我们要处理写进来的数据

  }
}

class WritableFile extends Writable {
  _write() {

  }
}

var myws = new Writable({
  highWaterMark: 20,
  write(chunk, encoding, done) {
    setTimeout(() => {
      console.log(chunk.toString())
      done()
    }, 500)
  }
})

var myrs = new Readable({
  highWaterMark: 20,
  read(size) {
    setTimeout(() => {
      var char = Math.random().toString().slice(2,3)
      this.push(char)
    }, 100);
  }
})

myrs.on('pause', () => {
  console.log('rs paused')
})

myrs.on('resume', () => {
  console.log('rs resumed')
})

myws.on('drain', () => {
  console.log('ws drained')
})

myrs.pipe(myws)


