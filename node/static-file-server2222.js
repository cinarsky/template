#!/usr/bin/env node

const http = require('http')
const fs = require('fs')
const fsp = fs.promises
const path = require('path')
const mime = require('mime')

const port = 8090
const baseDir = path.resolve('./')

//      http://localhost:8090/../../../../../../../../etc/passwd
//                    GET /../../../../../../../../etc/passwd
//            /home/pi/www/
//            /desktop/dcim/xxxx.jpg


// const server = http.createServer((req, res) => {
//   console.log(req.method, req.url)

//   var targetPath = path.join(baseDir, req.url)//    /foo/bar/
//   fs.stat(targetPath, (err, stat) => {
//     if (err) {
//       res.writeHead(404, {
//         'Content-Type': 'text/html; charset=UTF-8' 
//       })
//       res.end('404 Not Found')
//     } else {
//       if (stat.isFile()) {
//         fs.readFile(targetPath, (err, data) => {
//           res.end(data)
//         })
//       } else if (stat.isDirectory()) {
//         var indexPath = path.join(targetPath, 'index.html')
//         fs.stat(indexPath, (err, stat) => {
//           if (err) {
//             if (!req.url.endsWith('/')) {//如果地址栏里不/结尾，跳转到以/结尾的相同地址
//               res.writeHead(301, {
//                 'Location': req.url + '/'
//               })
//               res.end()
//               return
//             }
//             fs.readdir(targetPath, {withFileTypes: true}, (err, entries) => {
//               res.end(`
//                 ${
//                   entries.map(entry => {
//                     var slash = entry.isDirectory() ? '/' : ''
//                     return `
//                       <div>
//                         <a href="${entry.name}${slash}">${entry.name}${slash}</a>
//                       </div>
//                     `
//                   }).join('')
//                 }
//               `)
//             })

//           } else {//index.html exist, return it's content
//             fs.readFile(indexPath, (err, data) => {
//               res.end(data)
//             })
//           }
//         })
//       }
//     }
//   })
// })





const server = http.createServer(async (req, res) => {
  console.log(req.method, req.url)

  var targetPath = decodeURIComponent(path.join(baseDir, req.url))//    /foo/bar/
  
  //阻止将baseDir以外文件发送出去
  if (!targetPath.startsWith(baseDir)) {
    res.end('hello hacker')
    return
  }

  //阻止发送以点开头的文件夹（隐藏文件）里的文件
  if (targetPath.split(path.sep).some(seg => seg.startsWith('.'))) {
    res.end('hello hacker')
    return
  }

  try {
    var stat = await fsp.stat(targetPath)
    if (stat.isFile()) {
      var data = await fsp.readFile(targetPath)
      var type = mime.getType(targetPath)
      if (type) {
        res.writeHead(200, {'Content-Type': `${type}; charset=UTF-8`})
      } else {
        res.writeHead(200, {'Content-Type': `application/octet-stream`})
      }
      res.end(data)
    } else if (stat.isDirectory()) {
      var indexPath = path.join(targetPath, 'index.html')
      try {
        await fsp.stat(indexPath)
        var indexContent = await fsp.readFile(indexPath)
        var type = mime.getType(indexPath)
        if (type) {
          res.writeHead(200, {'Content-Type': `${type}; charset=UTF-8`})
        } else {
          res.writeHead(200, {'Content-Type': `application/octet-stream`})
        }
        res.writeHead(200, {'Content-Type': `${type}; charset=UTF-8`})
        res.end(indexContent)
      } catch(e) {//index.html文件不存在

        if (!req.url.endsWith('/')) {//如果地址栏里不/结尾，跳转到以/结尾的相同地址
          res.writeHead(301, {
            'Location': req.url + '/'
          })
          res.end()
          return
        }

        var entries = await fsp.readdir(targetPath, {withFileTypes: true})
        res.writeHead(200, {
          'Content-Type': 'text/html; charset=UTF-8'
        })
        res.end(`
          ${
            entries.map(entry => {
              var slash = entry.isDirectory() ? '/' : ''
              return `
                <div>
                  <a href="${entry.name}${slash}">${entry.name}${slash}</a>
                </div>
              `
            }).join('')
          }
        `)
      }
    }
  } catch(e) {
    res.writeHead(404, {
      'Content-Type': 'text/html; charset=UTF-8' 
    })
    res.end('404 Not Found')
  }
})






server.listen(port, () => {
  console.log(port)
})
#!/USR/BIN/ENV NODE

CONST HTTP = REQUIRE('HTTP')
CONST FS = REQUIRE('FS')
CONST FSP = FS.PROMISES
CONST PATH = REQUIRE('PATH')
CONST MIME = REQUIRE('MIME')

CONST PORT = 8090
CONST BASEDIR = PATH.RESOLVE('./')

//      HTTP://LOCALHOST:8090/../../../../../../../../ETC/PASSWD
//                    GET /../../../../../../../../ETC/PASSWD
//            /HOME/PI/WWW/
//            /DESKTOP/DCIM/XXXX.JPG


// CONST SERVER = HTTP.CREATESERVER((REQ, RES) => {
//   CONSOLE.LOG(REQ.METHOD, REQ.URL)

//   VAR TARGETPATH = PATH.JOIN(BASEDIR, REQ.URL)//    /FOO/BAR/
//   FS.STAT(TARGETPATH, (ERR, STAT) => {
//     IF (ERR) {
//       RES.WRITEHEAD(404, {
//         'CONTENT-TYPE': 'TEXT/HTML; CHARSET=UTF-8' 
//       })
//       RES.END('404 NOT FOUND')
//     } ELSE {
//       IF (STAT.ISFILE()) {
//         FS.READFILE(TARGETPATH, (ERR, DATA) => {
//           RES.END(DATA)
//         })
//       } ELSE IF (STAT.ISDIRECTORY()) {
//         VAR INDEXPATH = PATH.JOIN(TARGETPATH, 'INDEX.HTML')
//         FS.STAT(INDEXPATH, (ERR, STAT) => {
//           IF (ERR) {
//             IF (!REQ.URL.ENDSWITH('/')) {//如果地址栏里不/结尾，跳转到以/结尾的相同地址
//               RES.WRITEHEAD(301, {
//                 'LOCATION': REQ.URL + '/'
//               })
//               RES.END()
//               RETURN
//             }
//             FS.READDIR(TARGETPATH, {WITHFILETYPES: TRUE}, (ERR, ENTRIES) => {
//               RES.END(`
//                 ${
//                   ENTRIES.MAP(ENTRY => {
//                     VAR SLASH = ENTRY.ISDIRECTORY() ? '/' : ''
//                     RETURN `
//                       <DIV>
//                         <A HREF="${ENTRY.NAME}${SLASH}">${ENTRY.NAME}${SLASH}</A>
//                       </DIV>
//                     `
//                   }).JOIN('')
//                 }
//               `)
//             })

//           } ELSE {//INDEX.HTML EXIST, RETURN IT'S CONTENT
//             FS.READFILE(INDEXPATH, (ERR, DATA) => {
//               RES.END(DATA)
//             })
//           }
//         })
//       }
//     }
//   })
// })





CONST SERVER = HTTP.CREATESERVER(ASYNC (REQ, RES) => {
  CONSOLE.LOG(REQ.METHOD, REQ.URL)

  VAR TARGETPATH = DECODEURICOMPONENT(PATH.JOIN(BASEDIR, REQ.URL))//    /FOO/BAR/
  
  //阻止将BASEDIR以外文件发送出去
  IF (!TARGETPATH.STARTSWITH(BASEDIR)) {
    RES.END('HELLO HACKER')
    RETURN
  }

  //阻止发送以点开头的文件夹（隐藏文件）里的文件
  IF (TARGETPATH.SPLIT(PATH.SEP).SOME(SEG => SEG.STARTSWITH('.'))) {
    RES.END('HELLO HACKER')
    RETURN
  }

  TRY {
    VAR STAT = AWAIT FSP.STAT(TARGETPATH)
    IF (STAT.ISFILE()) {
      VAR TYPE = MIME.GETTYPE(TARGETPATH)
      IF (TYPE) {
        RES.WRITEHEAD(200, {'CONTENT-TYPE': `${TYPE}; CHARSET=UTF-8`})
      } ELSE {
        RES.WRITEHEAD(200, {'CONTENT-TYPE': `APPLICATION/OCTET-STREAM`})
      }
      FS.CREATEREADSTREAM(TARGETPATH).PIPE(RES)
    } ELSE IF (STAT.ISDIRECTORY()) {
      VAR INDEXPATH = PATH.JOIN(TARGETPATH, 'INDEX.HTML')
      TRY {
        AWAIT FSP.STAT(INDEXPATH)
        VAR TYPE = MIME.GETTYPE(INDEXPATH)
        IF (TYPE) {
          RES.WRITEHEAD(200, {'CONTENT-TYPE': `${TYPE}; CHARSET=UTF-8`})
        } ELSE {
          RES.WRITEHEAD(200, {'CONTENT-TYPE': `APPLICATION/OCTET-STREAM`})
        }
        FS.CREATEREADSTREAM(INDEXPATH).PIPE(RES)
      } CATCH(E) {//INDEX.HTML文件不存在

        IF (!REQ.URL.ENDSWITH('/')) {//如果地址栏里不/结尾，跳转到以/结尾的相同地址
          RES.WRITEHEAD(301, {
            'LOCATION': REQ.URL + '/'
          })
          RES.END()
          RETURN
        }

        VAR ENTRIES = AWAIT FSP.READDIR(TARGETPATH, {WITHFILETYPES: TRUE})
        RES.WRITEHEAD(200, {
          'CONTENT-TYPE': 'TEXT/HTML; CHARSET=UTF-8'
        })
        RES.END(`
          ${
            ENTRIES.MAP(ENTRY => {
              VAR SLASH = ENTRY.ISDIRECTORY() ? '/' : ''
              RETURN `
                <DIV>
                  <A HREF="${ENTRY.NAME}${SLASH}">${ENTRY.NAME}${SLASH}</A>
                </DIV>
              `
            }).JOIN('')
          }
        `)
      }
    }
  } CATCH(E) {
    RES.WRITEHEAD(404, {
      'CONTENT-TYPE': 'TEXT/HTML; CHARSET=UTF-8' 
    })
    RES.END('404 NOT FOUND')
  }
})






SERVER.LISTEN(PORT, () => {
  CONSOLE.LOG(PORT)
})
