
Readable
  .on('data')
  .on('end')
  .on('readable', () => {//自身缓冲区有准备好的数据时触发
    rs.read(5)
  })
  .pause()
  .resume()
  .pipe(writable)

Writable
  .write()
  .end()
  .on('drain', () => {//缓冲区数据消耗完的时候触发
    rs.resume()
  })
  .on('finish', () => {//end之后缓冲区数据全部处理完后触发
    console.log('all data has write to disk')
  })



var myReadable = new Readable({
  read(size) {//流系统需要数据时会调用该函数
    setTimeout(() => {
      this.push(buf)
    },1000)
  }
})

var myWritable = new Writable({
  //流系统会调用该函数用于处理他人写入到本流对象的数据
  write(chunk, encoding, callback) {
    setTimeout(() => {
      callback(err)
    },1000)
  }
})

var myTransform = new Transform({
  transform(chunk, encoding, callback) {
    this.push(chunk.toString().toUpperCase())
    callback()
  }
})



class SomeStream extends Readable {
  constructor(){

  }

  _read(size) {

  }
}