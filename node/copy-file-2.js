const { createReadStream, createWriteStream } = require('./file-read-stream')
const { Transform } = require('stream')




var myTransform = new Transform({
  transform(chunk, encoding, callback) {
    this.push(chunk.toString().toUpperCase())
    callback()
  }
})

createReadStream('./static-file-server.js')
.pipe(myTransform)
.pipe(createWriteStream('./static-file-server22223.js'))
