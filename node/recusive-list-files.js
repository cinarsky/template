const fs = require('fs')
const fsp = fs.promises

async function listAllFiles(path) {
  var result = []
  var stat = await fsp.stat(path)
  if (stat.isFile()) {
    return [path]
  } else {
    var entries = await fsp.readdir(path, {withFileTypes: true})

    for (let entry of entries) {
      var fullPath = path + '/' + entry.name
      if (entry.isFile()) {
        result.push(fullPath)
      } else {
        var files = await listAllFiles(fullPath)
        result.push(...files)
      }
    }

    return result
  }
}

async function listAllFilesBetter(path) {
  var result = []
  var stat = await fsp.stat(path)
  if (stat.isFile()) {
    return [path]
  } else {
    var entries = await fsp.readdir(path, {withFileTypes: true})

    var entryPromises = entries.map((entry, i) => {//启动所有子文件（夹）的读取
      var fullPath = path + '/' + entry.name
      return listAllFilesBetter(fullPath).then(files => {
        result[i] = files
      })
    })

    var entryValues = await Promise.all(entryPromises)

    // for (let entryPromise of entryPromises) {
    //   var files = await entryPromise
    //   result.push(...files)
    // }
    return [].concat(...result)
    return result.push(...[].concat(...entryValues))
  }
}

async function listAllFiles串行(path) {
  var result = []
  var stat = await fsp.stat(path)
  if (stat.isFile()) {
    return [path]
  } else {
    var entries = await fsp.readdir(path, {withFileTypes: true})

    for (let entry of entries) {
      var fullPath = path + '/' + entry.name
      var files = await listAllFiles串行(path)
      result.push(...files)
    }
    return [].concat(...result)
  }
}

async function listAllFiles并行等所有(path) {
  var result = []
  var stat = await fsp.stat(path)
  if (stat.isFile()) {
    return [path]
  } else {
    var entries = await fsp.readdir(path, {withFileTypes: true})

    //所有任务同时开发
    var entryPromises = entries.map(entry => {
      var fullPath = path + '/' + entry.name
      return listAllFiles并行等所有(path)
    })

    //串行等待每个任务，即便第二个完成了第一个没完成，也不能处理第二个
    for (let entryPromise of entryPromises) {
      var files = await entryPromise
      result.push(...files)
    }
    return [].concat(...result)
  }
}

function listAllFilesSync(path) {
  var result = []
  var stat = fs.statSync(path)
  if (stat.isFile()) {
    return [path]
  } else {
    var entries = fs.readdirSync(path, {withFileTypes: true})
    entries.forEach(entry => {
      var fullPath = path + '/' + entry.name
      if (entry.isFile()) {
        result.push(fullPath)
      } else {
        var files = listAllFilesSync(fullPath)
        result.push(...files)
      }
    })
    return result
  }
}

function listAllFilesPromise(path) {
  return fsp.stat(path).then(stat => {
    if (stat.isFile()) {
      return [path]
    } else {
      return fsp.readdir(path, {withFileTypes: true}).then(entries => {
        return Promise.all(entries.map(entry => {
          var fullPath = path + '/' + entry.name
          return listAllFilesPromise(fullPath)
        }))
      })
    }
  }).then(arrays => {
    return [].concat(...arrays)
  })
}

function listAllFilesCallback(path, callback) {
  fs.stat(path, (err, stat) => {
    if (stat.isFile()) {
      callback([path])
    } else {
      fs.readdir(path, {withFileTypes: true}, (err, entries) => {
        var result = []
        var count = 0

        if (entries.length == 0) {
          callback([])
        } else {
          entries.forEach((entry, i) => {
            var fullPath = path + '/' + entry.name

            listAllFilesCallback(fullPath, (files) => {
              result[i] = files
              count++
              if (count == entries.length) {
                callback([].concat(...result))
              }
            })
          })
        }
      })
    }
  })
}



console.log(0, listAllFilesSync('./aaa'))

listAllFilesPromise('./aaa').then(r => console.log(1, r))

listAllFilesCallback('./aaa', r => console.log(2, r))

listAllFiles('./aaa').then(r => console.log(3, r))


