const fs = require('fs')
const {Transform} = require('stream')


function split() {
  var lastHalfLine = ''

  return new Transform({
    objectMode: true,
    transform(chunk, encoding, callback) {
      var lines = chunk.toString().split('\r\n')
      if (lines.length > 1) {
        var lastLine = lines.pop()
        if (lastLine != '') {
          lastHalfLine = lastLine
        }
        lines.push(lastLine)
        this.push(lines[0] + lastHalfLine)
        lastHalfLine = ''
        for(var i = 1; i < lines.length; i++) {
          this.push(lines[i])
        }
        callback()
      } else {
        lastHalfLine = lines[0]
      }
    }
  })
}


fs.createReadStream('./static.js')
.pipe(split())
.on('data', line => {
  console.log(line)
  console.log('\n')
})