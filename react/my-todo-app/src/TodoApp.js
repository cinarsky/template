import React from 'react'
import { Provider } from 'react-redux'
import TodoInput from './TodoInput'
import TodoList from './TodoList'

import store from './store'
import url from './time.png'
import place from './do2b3HY.png'
import './a.css'


console.log('URL:::::', url)

export default function TodoApp() {
  return (
    <Provider store={store}>
      <div>
        <img src={place} alt="reddit place"/>
        <img src={url} alt="time"/>
        <TodoInput />
        <TodoList a={2}/>
      </div>
    </Provider>
  )
}
