var net = require('net')
var querystring = require('querystring')

var server = net.createServer()

var port = 8080


server.on('connection', conn => {
  console.log(conn.remoteAddress, conn.remotePort)
  conn.on('data', (data) => {
    // console.log('end')
    var request = data.toString()
    var [header, body] = request.split('\r\n\r\n')
    var [firstLine, ...headers] = header.split('\r\n')
    var [method, path] = firstLine.split(' ')

    var header = {}
    for(var h of headers) {
      var [key, val] = h.split(': ')
      header[key.toLowerCase()] = val
    }

    if (path == '/author') {

      if (header['accept'] == 'text/html') {
        conn.write('HTTP/1.1 200 OK\r\n')
        conn.write('Content-Type: text/html; charset=UTF-8\r\n')
        if (header.referer.startsWith('http://www.baidu.com') || header.referer.startsWith('https://www.baidu.com')) {
          conn.write('Access-Control-Allow-Origin: *\r\n')
        }
        conn.write('\r\n')
        conn.end(`<div>I'm the author</div>`)
      }
      if (header['accept'] == 'text/plain') {
        conn.write('HTTP/1.1 200 OK\r\n')
        conn.write('Content-Type: text/plain; charset=UTF-8\r\n')
        conn.write('Access-Control-Allow-Origin: *\r\n')
        conn.write('\r\n')
        conn.end(`I'm the author`)
      }
      if (header['accept'] == 'application/json') {
        conn.write('HTTP/1.1 200 OK\r\n')
        conn.write('Access-Control-Allow-Origin: *\r\n')
        conn.write('Content-Type: application/json\r\n')
        conn.write('\r\n')
        conn.end(`{"info": "I'm the author"}`)
      }
    }

    if (path == '/') {
      conn.write('HTTP/1.1 200 OK\r\n')
      conn.write('\r\n')
      conn.end('hello world')
    }

  })
})
 
server.listen(port, () => {
  console.log('server listening on port', port)
})



// HTTP/1.1 200 OK//首部
// Content-Type: text/html
// Content-Length: 892
// Date: 

// 头